<?php 
class Post {
    private $conn;
    public $table = 'posts';
    public $user_table = 'users';

  // Columns 
    public $post_id;
    public $post_category_id;
    public $cat_title;
    public $post_title;
    public $post_content;
    public $post_author;
    public $post_date;


// Constructor with DB

public function __construct($db) {
    $this->conn = $db;
}

// Get Posts
public function read() {
    // create Query
    // $query = 'SELECT *FROM '.$this->table .' JOIN categories ON post_category_id = cat_id'; 

    $query = "SELECT
    c.cat_title, p.post_id, p.post_title, p.post_content, p.post_author, p.post_category_id, p.post_date
FROM
    " . $this->table . " p
    LEFT JOIN
        categories c
            ON p.post_category_id = c.cat_id
ORDER BY
    p.post_date DESC";
  
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // execute query
    $stmt->execute();
 
    return $stmt;

    
}


// Read one
public function read_single() {
    $query = "SELECT
    c.cat_title, p.post_id, p.post_title, p.post_content, p.post_author, p.post_category_id, p.post_date
FROM
    " . $this->table . " p
    LEFT JOIN
        categories c
            ON p.post_category_id = c.cat_id
WHERE
    p.post_id = ?
LIMIT
    0,1";

    $stmt = $this->conn->prepare($query);

    // BIND ID
    $stmt ->bindParam(1, $this->post_id);

    // Execute Query
    $stmt->execute();

    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
    $this->post_title = $row['post_title'];
    $this->post_category_id = $row['post_category_id'];
    $this->post_content = $row['post_content'];
    $this->post_author = $row['post_author'];
    $this->post_date = $row['post_date'];
    $this->category_name = $row['cat_title'];
}


// Create

 public function create(){
 
    // query to insert record
    $query = "INSERT INTO
                " . $this->table . "
            SET
                post_title=:post_title,
                post_category_id=:post_category_id,
                post_content=:post_content,
                post_author=:post_author, 
                post_date=:post_date";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // making data clean
    $this->post_title=htmlspecialchars(strip_tags($this->post_title));
    $this->post_category_id=htmlspecialchars(strip_tags($this->post_category_id));
    $this->post_content=htmlspecialchars(strip_tags($this->post_content));
    $this->post_author=htmlspecialchars(strip_tags($this->post_author));
    $this->post_date=htmlspecialchars(strip_tags($this->post_date));
 
    // bind values
    $stmt->bindParam(":post_title", $this->post_title);
    $stmt->bindParam(":post_category_id", $this->post_category_id);
    $stmt->bindParam(":post_content", $this->post_content);
    $stmt->bindParam(":post_author", $this->post_author);
    $stmt->bindParam(":post_date", $this->post_date);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
    return false;
     
}

// user Create here 
 public function create_user(){
 
    // query to insert record
    $query = "INSERT INTO
                " . $this->user_table . "
            SET
                username=:username,
                user_password=:user_password,
                user_email=:user_email";
            
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // making data clean
    $this->username=htmlspecialchars(strip_tags($this->username));
    $this->user_password=htmlspecialchars(strip_tags($this->user_password));
    $this->user_email=htmlspecialchars(strip_tags($this->user_email));

 
    // bind values
    $stmt->bindParam(":username", $this->username);
    $stmt->bindParam(":user_password", $this->user_password);
    $stmt->bindParam(":user_email", $this->user_email);

    // hashing password
    $password_hash = password_hash($this->user_password, PASSWORD_BCRYPT);
    $stmt->bindParam(':user_password', $password_hash);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
    return false;
     
}
  

// Update
public function update(){
 
    // query to insert record
    $query = "UPDATE
                " . $this->table . "
            SET
                post_title=:post_title,
                post_category_id=:post_category_id,
                post_content=:post_content,
                post_author=:post_author, 
                post_date=:post_date
                WHERE 
                post_id = :post_id";
 
    // prepare query
    $stmt = $this->conn->prepare($query);
 
    // making data clean
    $this->post_title=htmlspecialchars(strip_tags($this->post_title));
    $this->post_id=htmlspecialchars(strip_tags($this->post_id));
    $this->post_category_id=htmlspecialchars(strip_tags($this->post_category_id));
    $this->post_content=htmlspecialchars(strip_tags($this->post_content));
    $this->post_author=htmlspecialchars(strip_tags($this->post_author));
    $this->post_date=htmlspecialchars(strip_tags($this->post_date));
 
    // bind values
    $stmt->bindParam(":post_title", $this->post_title);
    $stmt->bindParam(":post_id", $this->post_id);
    $stmt->bindParam(":post_category_id", $this->post_category_id);
    $stmt->bindParam(":post_content", $this->post_content);
    $stmt->bindParam(":post_author", $this->post_author);
    $stmt->bindParam(":post_date", $this->post_date);
 
    // execute query
    if($stmt->execute()){
        return true;
    }
    return false;
     
}

// DELETE
public function delete() {
    $query = "DELETE FROM " . $this->table . " WHERE post_id = ?";

    // Prepare Statement
    $stmt = $this->conn->prepare($query);

    $this->post_id = htmlspecialchars(strip_tags($this->post_id));
    // $stmt->bindParam(':post_id', $this->post_id);
    $stmt->bindParam(1, $this->id);

    if($stmt->execute()){
        return true;
    }
    return false;
}

}




?>