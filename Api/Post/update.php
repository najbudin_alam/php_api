<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers,Access-Control-Allow-Methods Authorization, X-Requested-With");
 
require_once '../../config/Database.php';

include_once './validate_token.php';

// obj
include_once '../../models/Post.php';
 

$database = new Database();
$db = $database->connect();
 
$post = new Post($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));


// update id
$post->post_id = $data->post_id;

 
    $post->post_title = $data->post_title;
    $post->post_content = $data->post_content;
    $post->post_category_id = $data->post_category_id;
    $post->post_author = $data->post_author;
    $post->post_date = date('Y-m-d');

 
    // Update  post
    if($post->update()){

        echo json_encode(array("message" => "post  Updated."));
    }
 
    else{
        echo json_encode(array("message" => "Unable to Update post."));
    }

?>