<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

require_once '../../config/Database.php';
include_once '../../models/Post.php';

include_once './validate_token.php';

// Instinaciating Database

$database = new Database();
$db = $database->connect();

// Instancinate Blog Posts

$post = new Post($db);
// Why $db ? -> when we created constructor in Posts.php model we used $db so need to pass here.

// Get ID
$post->post_id = isset($_GET['post_id']) ? $_GET['post_id'] : die();

// Get POST
$post->read_single();

// Create array
$post_arr = array (
    'post_id' => $post ->post_id,
    'post_title' => $post ->post_title,
    'post_content' => $post ->post_content,
    'post_author' => $post ->post_author,
    'post_date' => $post ->post_date,
    'post_category_id' => $post ->post_category_id,
    'cat_title' => $post ->category_name
);

// Make JSON

print_r(json_encode($post_arr));

?>