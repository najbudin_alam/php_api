<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers,Access-Control-Allow-Methods Authorization, X-Requested-With");
 
require_once '../../config/Database.php';

// obj
include_once '../../models/Post.php';
 

$database = new Database();
$db = $database->connect();
 
$post = new Post($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// make sure data is not empty
if(
   
    !empty($data->username) &&
    !empty($data->user_password) &&
    !empty($data->user_email)
 
){
 
    // set post ko values
    $post->username = $data->username;
    $post->user_password = $data->user_password;
    $post->user_email = $data->user_email;

 
    // create  post
    if($post->create_user()){

        echo json_encode(array("message" => "User Registered Successfully."));
    }
 
    else{
        echo json_encode(array("message" => "Unable to create user."));
    }
}
 
else{
    echo json_encode(array("message" => "Unable to create user. Data is incomplete."));
}
?>