<?php 
// generate json web token
include_once '../../config/core.php';
include_once '../../libs/php-jwt-master/src/BeforeValidException.php';
include_once '../../libs/php-jwt-master/src/ExpiredException.php';
include_once '../../libs/php-jwt-master/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

// echo "<pre>";

// var_dump($_SERVER['HTTP_AUTHORIZATION']) [1];

// echo "</pre>";

 $jwt = explode(" ", $_SERVER['HTTP_AUTHORIZATION']) [1] ?? false;


//  jwt  emptyness check

if($jwt){
 
    try {
        // decode jwt
        $decoded = JWT::decode($jwt, $key, array('HS256'));
 
        echo json_encode(array(
            "message" => "Access granted.",
            "data" => $decoded->data
        ));
 
    }catch(Exception $e) {
        exit(
            json_encode(array(
                "message" => "Access denied.",
                "error" => $e->getMessage()
        ) 
        ));
    }

}else{
    exit(
        json_encode(array("message" => "Access denied."))
    ) ;
}

?>