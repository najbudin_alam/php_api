<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers,Access-Control-Allow-Methods Authorization, X-Requested-With");
 
require_once '../../config/Database.php';

// obj
include_once '../../models/Post.php';
 

$database = new Database();
$db = $database->connect();
 
$post = new Post($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));
 
// make sure data is not empty
if(
   
    !empty($data->post_title) &&
    !empty($data->post_content) &&
    !empty($data->post_category_id) &&
    !empty($data->post_date) &&
    !empty($data->post_author)
){
 
    // set post ko values
    $post->post_title = $data->post_title;
    $post->post_content = $data->post_content;
    $post->post_category_id = $data->post_category_id;
    $post->post_author = $data->post_author;
    $post->post_date = date('Y-m-d');

 
    // create  post
    if($post->create()){

        echo json_encode(array("message" => "post  created."));
    }
 
    else{
        echo json_encode(array("message" => "Unable to create post."));
    }
}
 
else{
    echo json_encode(array("message" => "Unable to create post. Data is incomplete."));
}
?>