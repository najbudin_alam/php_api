<?php 

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header("Access-Control-Allow-Methods: GET");

 include_once './validate_token.php';

require_once '../../config/Database.php';
include_once '../../models/Post.php';


// Instinaciating Database
$database = new Database();
$db = $database->connect();


// Instancinate Blog Posts

$post = new Post($db);

// Why $db ? -> when we created constructor in Posts.php model we used $db so need to pass here.

// Blog post Query
$result = $post->read();

// Get row count

$num = $result->rowCount();

// Check If any Posts

if($num > 0) {
    // initializing Aray
    $post_arr = array();
    $post_arr['data'] = array();

    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $post_item = array(
            'id' => $post_id,
            'title' => $post_title,
            'content' => $post_content,
            'cat_id' => $post_category_id,
            'category_name' => $cat_title

        );

        // push to "data"

        array_push($post_arr['data'], $post_item);

    }

    // Turn Json and Output
    echo json_encode($post_arr);

} else {
    // No posts
    echo json_encode(
        array('message' => 'No Posts Found')
    ); 
}


?>