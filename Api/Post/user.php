<?php
    class Users{

        //define properties
        public $id;
        public $username;
        public $user_email;
        public $user_password;

        private $conn;
        private $users_table;


        public function __construct($db){

            $this->conn = $db;
            $this->users_table = "users";
        }


        public function check_email(){

            // query to check if email exists
            $query = "SELECT username, user_password
            FROM " . $this->users_table . "
            WHERE user_email = ?
            LIMIT 0,1";

            $stmt = $this->conn->prepare( $query );

            $this->user_email=htmlspecialchars(strip_tags($this->user_email));

            $stmt->bindParam(1, $this->user_email);

            $stmt->execute();

            $num = $stmt->rowCount();

            if($num>0){
 
                // TO get record details 
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
         
           
                $this->username = $row['username'];
                $this->user_password = $row['user_password'];
         
                return true;
            }
            return false;
        }
    }