<?php 
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers,Access-Control-Allow-Methods Authorization, X-Requested-With");

// generate json web token
include_once '../../libs/php-jwt-master/src/BeforeValidException.php';
include_once '../../libs/php-jwt-master/src/ExpiredException.php';
include_once '../../libs/php-jwt-master/src/SignatureInvalidException.php';
include_once '../../libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;
 
require_once '../../config/Database.php';
include_once '../../config/core.php';

// obj
include_once './user.php';
 

$database = new Database();
$db = $database->connect();
 
$user = new Users($db);
 
// get user posted data
$data = json_decode(file_get_contents("php://input"));

 
// set product property values
$user->user_email = $data->user_email;
$user->user_password = $data->user_password;
$email_exists = $user->check_email();


// Generate JWT authentication
if($email_exists && password_verify($data->user_password, $user->user_password)){
 
    $token = array(
       "iat" => $issued_at,
       "exp" => $expiration_time,
       "iss" => $issuer,
       "data" => array(
           "username" => $user->username,
           "user_email" => $user->user_email
       )
    );
 
 
    // generate jwt
    $jwt = JWT::encode($token, $key);
    echo json_encode(
            array(
                "message" => "Successful login.",
                "jwt" => $jwt
            )
        );
 
}else{
 
    //  user ko login failed
    echo json_encode(array("message" => "Login failed."));
}

?>